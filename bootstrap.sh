$!/bin/bash

export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get install -y build-essential liblzma-dev liblzo2-dev zlib1g-dev
apt-get install -y binwalk git
git clone https://github.com/devttys0/sasquatch.git
cd sasquatch
./build.sh
